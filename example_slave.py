from py_nwtraffic_slave import *
from py_nwtraffic_common import *

master_ip_address = "<master-ip-addr>"
slave_name = "Any name identifier"
slave = py_nwtraffic_slave_t(slave_name, master_ip_address)
slave.start()
