import socket
import os
import sys
from select import select
from sys import stderr
from os import urandom
from random import randint, randrange, uniform
from math import floor, ceil, sin, pi
from pdb import set_trace
from threading import Thread, Event
from time import sleep, ctime, time
from socket import AF_INET, SOCK_STREAM, SOCK_DGRAM
from struct import pack, unpack
from pickle import dumps, loads
from py_nwtraffic_ctrl import *

def screen_clear():
    print("\033c")

def screen_cursor_reset():
    print("\033[H")

class tcpSocketClosed(Exception):
    pass


NUM_IGNORE_SLAVE_MISSED_ALIVE_MSGS = 50
BEACON_MSG_INTERVAL = 2 # Seconds
SLAVE_WAIT_SLEEP_TIME = 0.1
SOCKET_DATA_PACK_FORMAT_SPEC  = "I" # As unsigned integer
SOCKET_DATA_PACKET_TIME_INTERVAL = 1/1000 # Seconds
TRANSFER_SLEEP_INTERVAL = 1/1000
DATA_RATE_SCALE     = 1.00
SOCKET_RECV_TIMEOUT = 1 # Seconds
SOCKET_CONNECT_TIMEOUT = 10
SLAVE_ALIVE_MESSAGE_INTERVAL = 1 # Seconds
SOCKET_MAX_PACKET_SIZE = 0xFFFF - 100
DATA_MAX_PACKET_SIZE = SOCKET_MAX_PACKET_SIZE - data_msg_pack_size - calcsize(SOCKET_DATA_PACK_FORMAT_SPEC)
DATA_RATE_MONITOR_INTERVAL = 0.5 # Seconds
SOCKET_CTRL_TIMEOUT = 10
SOCKET_UNLIMITED_TIMEOUT = 0x7FFF
SOCKET_UDP_TIMEOUT = 1
SOCKET_TCP_TIMEOUT = 1

SOCKET_CONNECT_DELAY = 0.3

ETHERNET_OVERHEAD  = 14+4
UDP_OVERHEAD = 8
TCP_OVERHEAD = 20
IP_PACKET_OVERHEAD = 20

class data_transfer_properties_enum_t(Enum):
    SINE_WAVE_FREQ = 1 # In Hz
    TRIANGLE_WAVE_FREQ = 1 # In Hz

class connection_port_enum_t(Enum):
    # Control channel ports
    SLAVE_CONNECT_RECV_PORT_UDP     = 3000
    MASTER_CONNECT_RECV_PORT_UDP    = 3001
    SLAVE_CTRL_PORT                 = 3002
    SLAVE_LOG_PORT                  = 3003
    SLAVE_ALIVE_PORT                = 3004 

    # Data channel ports, TCP
    SLAVE_DL_DATA_PORT_TCP          = 4000
    MASTER_UL_DATA_PORT_TCP         = 5000

    # Data channel ports, UDP 
    SLAVE_DL_DATA_PORT_UDP          = 4000
    MASTER_UL_DATA_PORT_UDP         = 5000

    # Beacon msg ports, UDP
    MASTER_UDP_BEACON_MSG_RECV_PORT     = 6000

    # TCP socket for IP address connection
    MASTER_IP_ADDR_CONN_PORT_TCP    = 2000
    MASTER_IP_ADDR_CONN_LISTEN_COUNT = 1000

    @staticmethod
    def get_slave_data_ports(slave_idx, slave_transfer_idx):
        offset_number = slave_idx*10 + slave_transfer_idx
        assert offset_number<1000

        master_ul_recv_port_udp = connection_port_enum_t.MASTER_UL_DATA_PORT_UDP + offset_number
        master_ul_recv_port_tcp = connection_port_enum_t.MASTER_UL_DATA_PORT_TCP + offset_number
       
        slave_dl_recv_port_udp = connection_port_enum_t.SLAVE_DL_DATA_PORT_UDP + offset_number
        slave_dl_recv_port_tcp = connection_port_enum_t.SLAVE_DL_DATA_PORT_TCP + offset_number

        return (master_ul_recv_port_udp, master_ul_recv_port_tcp,
                slave_dl_recv_port_udp, slave_dl_recv_port_tcp)

py_nwtraffic_master_logging_period = 10 # Seconds
py_nwtraffic_slave_logging_period = 10 # Seconds

class py_nwtraffic_logger_t:
    def __init__(self, is_master):
        dir_path = os.path.dirname(os.path.abspath(sys.argv[0]))
        if is_master:
            self.log_file = open(dir_path+"/master_log.txt", "a")
        else:
            self.log_file = open(dir_path+"/slave_log.txt", "a")
        
        self.log_file.write("\n\n\nNew Session\n")

    def log(self, msg):
        self.log_file.write(ctime() + " : " + msg + "\n")
        self.log_file.flush()

g_logger_obj = None

class py_nwtraffic_slave_property_t : 
    def __init__(self):
        self.idx = None
        self.is_connected = False
        self.ip_addr = None
        self.ctrl_channel = None
        self.logging_channel = None
        self.alive_channel = None
        self.transfer_ref = dict()
        self.log = None

    def disconnect(self):
        global disconnect
        disconnect(self.ctrl_channel)
        disconnect(self.logging_channel)
        disconnect(self.alive_channel)
        self.is_connected = False

class py_nwtraffic_master_property_t :
    def __init__(self):
        self.is_connected = False
        self.keep_alive_thread = None 
        self.logging_thread = None 
        self.ip_addr = None 
        self.ctrl_channel = None
        self.dl_data_channel_tcp = None
        self.ul_data_channel_tcp = None
        self.dl_data_channel_udp = None
        self.ul_data_channel_udp = None
        self.logging_channel = None
        self.alive_channel = None
        self.transfer_ref = dict()
        self.log = None

class py_nwtraffic_transfer_t:
    def __init__(self, is_master, master_slave_ref):
        self.is_master = is_master
        self.transfer_idx = None
        self.is_dual_transfer = False
        self.dl_transfer_idx = None
        self.dl_transfer_protocol = None
        self.dl_transfer_direction = None
        self.dl_transfer_duration = None
        self.dl_transfer_rate = None
        self.dl_transfer_thread = None
        self.dl_transfer_thread_event = Event() # Event to track if the thread is running
        self.dl_inst_data_rate = 0
        self.dl_reported_inst_data_rate = 0
        self.dl_data_channel_tcp = None
        self.dl_data_channel_udp = None
        self.ul_transfer_idx = None
        self.ul_transfer_protocol = None
        self.ul_transfer_direction = None
        self.ul_transfer_duration = None
        self.ul_transfer_rate = None
        self.ul_transfer_thread = None
        self.ul_transfer_thread_event = Event() # Event to track if the thread is running 
        self.ul_inst_data_rate = 0
        self.ul_reported_inst_data_rate = 0
        self.ul_data_channel_tcp = None
        self.ul_data_channel_udp = None
        if is_master:
            self.slave = master_slave_ref
        else:
            self.master = master_slave_ref

    def connect_data_channels(self):
        all_ip = "0.0.0.0"
        if self.is_master:
            (master_ul_recv_port_udp, master_ul_recv_port_tcp, 
                    slave_dl_recv_port_udp, slave_dl_recv_port_tcp) = \
                        connection_port_enum_t.get_slave_data_ports(self.slave.idx, self.transfer_idx)
            data_connect_req = data_connect_req_t(
                slave_idx = self.slave.idx,
                transfer_idx = self.transfer_idx,
                req_time = ctime(),
                master_ul_recv_port_udp = master_ul_recv_port_udp,
                master_ul_recv_port_tcp = master_ul_recv_port_tcp,
                slave_dl_recv_port_udp = slave_dl_recv_port_udp,
                slave_dl_recv_port_tcp = slave_dl_recv_port_tcp,)
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = data_connect_req)
            send_message_tcp(self.slave.ctrl_channel, py_nwtraffic_msg, 0)
            reply = recv_message_tcp(self.slave.ctrl_channel, SOCKET_CTRL_TIMEOUT)
            assert reply
            connect_rsp = reply.msg_content
            assert connect_rsp.slave_ack == connect_rsp_ack_enum_t.CONNECT_ACCEPT

            is_receive = True
            self.ul_data_channel_udp = create_connection_udp(all_ip, master_ul_recv_port_udp, is_receive, 5)
            sleep(SOCKET_CONNECT_DELAY)
            self.ul_data_channel_tcp = create_connection_tcp(self.slave.ip_addr, master_ul_recv_port_tcp, 5)
            sleep(SOCKET_CONNECT_DELAY)
            is_receive = False
            self.dl_data_channel_udp = create_connection_udp(self.slave.ip_addr, \
                    slave_dl_recv_port_tcp, is_receive, 5)
            sleep(SOCKET_CONNECT_DELAY)
            self.dl_data_channel_tcp = create_connection_tcp(self.slave.ip_addr, slave_dl_recv_port_tcp, 5)
            sleep(SOCKET_CONNECT_DELAY)

            self.dl_data_channel_tcp.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)
            self.ul_data_channel_tcp.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)
        else:
            msg = recv_message_tcp(self.master.ctrl_channel, SOCKET_CTRL_TIMEOUT)
            assert msg
            data_connect_req = msg.msg_content

            master_ul_recv_port_udp = data_connect_req.master_ul_recv_port_udp
            master_ul_recv_port_tcp = data_connect_req.master_ul_recv_port_tcp
            slave_dl_recv_port_udp = data_connect_req.slave_dl_recv_port_udp
            slave_dl_recv_port_tcp = data_connect_req.slave_dl_recv_port_tcp

            data_connect_rsp = data_connect_resp_t(
                    slave_idx = data_connect_req.slave_idx,
                    slave_ack = connect_rsp_ack_enum_t.CONNECT_ACCEPT,
                    rsp_time = ctime())

            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = data_connect_rsp)

            send_message_tcp(self.master.ctrl_channel, py_nwtraffic_msg, 0)
            

            is_receive = False
            self.ul_data_channel_udp = \
                create_connection_udp(self.master.ip_addr, master_ul_recv_port_udp, is_receive, SOCKET_RECV_TIMEOUT)
            [self.ul_data_channel_tcp, _] = \
                accept_connection(all_ip, master_ul_recv_port_tcp, SOCKET_RECV_TIMEOUT)
            is_receive = True
            self.dl_data_channel_udp    =  \
                create_connection_udp(all_ip, slave_dl_recv_port_udp, is_receive, SOCKET_RECV_TIMEOUT)
            [self.dl_data_channel_tcp, _]    =  \
                accept_connection(all_ip, slave_dl_recv_port_tcp, SOCKET_RECV_TIMEOUT)

            self.dl_data_channel_tcp.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)
            self.ul_data_channel_tcp.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)

    def init(self, transfer_idx = None, transfer_protocol = None, transfer_direction = None,
            traffic_type = None, transfer_duration = None, transfer_rate = None,
            mtu_size = None):
        assert transfer_idx != None
        assert transfer_protocol != None
        assert transfer_direction != None
        assert traffic_type != None
        assert transfer_duration != None
        assert transfer_rate != None
        assert mtu_size != None
        self.mtu_size = mtu_size
        if self.is_master:

            self.transfer_idx = transfer_idx

            if transfer_direction==data_direction_enum_t.MASTER_TO_SLAVE \
                    or transfer_direction==data_direction_enum_t.BOTH_TO_BOTH:
                assert self.dl_transfer_thread==None
                self.dl_transfer_idx = transfer_idx
                self.dl_transfer_protocol = transfer_protocol
                self.dl_traffic_type = traffic_type
                self.dl_transfer_duration = transfer_duration
                self.dl_transfer_rate = transfer_rate
                self.dl_transfer_thread = Thread(target=self._transfer_send_data_thread)
            if transfer_direction==data_direction_enum_t.SLAVE_TO_MASTER \
                    or transfer_direction==data_direction_enum_t.BOTH_TO_BOTH:
                assert self.ul_transfer_thread==None
                self.ul_transfer_idx = transfer_idx
                self.ul_transfer_protocol = transfer_protocol
                self.ul_traffic_type = traffic_type
                self.ul_transfer_duration = transfer_duration
                self.ul_transfer_rate = transfer_rate
                self.ul_transfer_thread = Thread(target=self._transfer_recieve_data_thread)
            
            if transfer_direction==data_direction_enum_t.BOTH_TO_BOTH:
                self.is_dual_transfer = True

            transfer_req_msg = transfer_req_msg_t(
                transfer_idx = transfer_idx,
                data_protocol = transfer_protocol,
                data_direction = transfer_direction,
                data_transfer_type = traffic_type,
                data_duration = transfer_duration,
                data_transfer_rate = transfer_rate,
                max_buffer_size = ceil(transfer_rate*SOCKET_DATA_PACKET_TIME_INTERVAL)*2,
                mtu_size = mtu_size)
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_req_msg)
            send_message_tcp(self.slave.ctrl_channel, py_nwtraffic_msg, 0)
            return recv_message_tcp(self.slave.ctrl_channel, SOCKET_CTRL_TIMEOUT)
        else:
            assert transfer_idx != None
            assert transfer_protocol != None
            assert transfer_direction != None
            assert traffic_type != None
            assert transfer_duration != None
            assert transfer_rate != None

            self.transfer_idx = transfer_idx

            if transfer_direction==data_direction_enum_t.MASTER_TO_SLAVE \
                    or transfer_direction==data_direction_enum_t.BOTH_TO_BOTH:
                assert self.ul_transfer_thread==None
                self.ul_transfer_idx = transfer_idx
                self.ul_transfer_protocol = transfer_protocol
                self.ul_traffic_type = traffic_type
                self.ul_transfer_duration = transfer_duration
                self.ul_transfer_rate = transfer_rate
                self.ul_transfer_thread = Thread(target=self._transfer_recieve_data_thread)
            if transfer_direction==data_direction_enum_t.SLAVE_TO_MASTER \
                    or transfer_direction==data_direction_enum_t.BOTH_TO_BOTH:
                assert self.dl_transfer_thread==None
                self.dl_transfer_idx = transfer_idx
                self.dl_transfer_protocol = transfer_protocol
                self.dl_traffic_type = traffic_type
                self.dl_transfer_duration = transfer_duration
                self.dl_transfer_rate = transfer_rate
                self.dl_transfer_thread = Thread(target=self._transfer_send_data_thread)
            self.transfer_action = transfer_action_enum_t.TRANSFER_INIT
            transfer_ack_msg = transfer_ack_msg_t(
                    transfer_idx,
                    self.transfer_action,
                    ctime(),
                    transfer_action_ack_enum_t.TRANSFER_SUCCESS,
                    self.dl_inst_data_rate,
                    self.ul_inst_data_rate)
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_ack_msg)
            send_message_tcp(self.master.ctrl_channel, py_nwtraffic_msg, 0)
            return


    def transfer_start(self):
        if self.is_master:
            self.transfer_action = transfer_action_enum_t.TRANSFER_START
            transfer_ctrl_msg = transfer_ctrl_msg_t(
                    transfer_idx = self.transfer_idx, 
                    transfer_action = self.transfer_action,
                    transfer_action_time = ctime())
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_ctrl_msg)
            send_message_tcp(self.slave.ctrl_channel, py_nwtraffic_msg, 0)
            reply = recv_message_tcp(self.slave.ctrl_channel, SOCKET_CTRL_TIMEOUT)
            assert reply.msg_content.transfer_idx==transfer_ctrl_msg.transfer_idx
            assert reply.msg_content.transfer_action==transfer_ctrl_msg.transfer_action
            self._transfer_threads_start()
            return reply
        else:
            self.transfer_action = transfer_action_enum_t.TRANSFER_START
            transfer_ack_msg = transfer_ack_msg_t(
                    self.transfer_idx,
                    self.transfer_action,
                    ctime(),
                    transfer_action_ack_enum_t.TRANSFER_SUCCESS,
                    self.dl_inst_data_rate,
                    self.ul_inst_data_rate)
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_ack_msg)
            send_message_tcp(self.master.ctrl_channel, py_nwtraffic_msg, 0)
            self._transfer_threads_start()
            return

    def _transfer_send_data_thread(self):
        if self.is_master:
            g_logger_obj.log("Data transfer thread started for {0}".format(self.slave.ip_addr))
            if self.dl_transfer_protocol==data_protocol_type_enum_t.UDP_TRAFFIC:
                send = send_message_udp
                sock = self.dl_data_channel_udp
            elif self.dl_transfer_protocol==data_protocol_type_enum_t.TCP_TRAFFIC:
                send = send_message_tcp
                sock = self.dl_data_channel_tcp
            else:
                assert 0
            [sleep_time_ref, packets_list, dummy_packet, overhead] = gen_packets(self.dl_transfer_rate,
                    self.dl_transfer_duration, self.dl_traffic_type, self.dl_transfer_protocol,
                    self.mtu_size)
            g_logger_obj.log("Sending {0} packets of size {1} using sleep of {2}".format(len(packets_list),
                packets_list[0], sleep_time_ref))
            seq_idx = 0
            start_time = time()
            delta_time = sleep_time_ref
            sum_data_len = 0
            sum_delta_time = 0
            self.dl_transfer_thread_event.set()
            idx = 0
            for idx in range(len(packets_list)):
                send_start = time()
                msg = data_msg_t(seq_idx = seq_idx, cur_time = ctime(),
                        data_bytes = dummy_packet[0:packets_list[idx]])
                try:
                    data_len = send(sock, msg, 0, is_data_packet=True) + overhead
                except:
                    g_logger_obj.log("Stopping data transfer since socket error in send")
                    break
                seq_idx += 1
                if not self.dl_transfer_thread_event.is_set():
                    g_logger_obj.log("Stopping data transfer since event was set")
                    break
                while time() < start_time + delta_time*(idx+1):
                    sleep(TRANSFER_SLEEP_INTERVAL)
                send_end = time()
                if sum_delta_time > DATA_RATE_MONITOR_INTERVAL:
                    self.dl_inst_data_rate = sum_data_len/sum_delta_time
                    sum_data_len = 0
                    sum_delta_time = 0
                else:
                    sum_data_len += data_len
                    sum_delta_time += (send_end - send_start)
                sleep(0)    # yield to other thread
            g_logger_obj.log("Data transfer completed {0} of {1} packets/chunks".format(idx, len(packets_list)))
            self.dl_transfer_thread_event.clear()
            g_logger_obj.log("Data transfer thread stopped for {0}".format(self.slave.ip_addr))
        else:
            g_logger_obj.log("Data transfer thread started for {0}".format(self.master.ip_addr))
            if self.dl_transfer_protocol==data_protocol_type_enum_t.UDP_TRAFFIC:
                send = send_message_udp
                sock = self.ul_data_channel_udp
            elif self.dl_transfer_protocol==data_protocol_type_enum_t.TCP_TRAFFIC:
                send = send_message_tcp
                sock = self.ul_data_channel_tcp
            else:
                assert 0
            [sleep_time_ref, packets_list, dummy_packet, overhead] = gen_packets(self.dl_transfer_rate,
                    self.dl_transfer_duration, self.dl_traffic_type, self.dl_transfer_protocol,
                    self.mtu_size)
            g_logger_obj.log("Sending {0} packets of size {1} using sleep of {2}".format(len(packets_list),
                packets_list[0], sleep_time_ref))
            seq_idx = 0
            start_time = time()
            delta_time = sleep_time_ref
            sum_data_len = 0
            sum_delta_time = 0
            self.dl_transfer_thread_event.set()
            for idx in range(len(packets_list)):
                send_start = time()
                msg = data_msg_t(seq_idx = seq_idx, cur_time = ctime(),
                        data_bytes = dummy_packet[0:packets_list[idx]])
                try:
                    data_len = send(sock, msg, 0, is_data_packet=True) + overhead
                except:
                    break
                seq_idx += 1
                if not self.dl_transfer_thread_event.is_set():
                    print("Stopping data transfer since event was set")
                    break
                while time() < start_time + delta_time*(idx+1):
                    sleep(TRANSFER_SLEEP_INTERVAL)
                send_end = time()
                if sum_delta_time > DATA_RATE_MONITOR_INTERVAL:
                    self.ul_inst_data_rate = sum_data_len/sum_delta_time
                    sum_data_len = 0
                    sum_delta_time = 0
                else:
                    sum_data_len += data_len
                    sum_delta_time += (send_end - send_start)
                sleep(0)    # yield to other thread
            self.dl_transfer_thread_event.clear()
            g_logger_obj.log("Data transfer thread stopped for {0}".format(self.master.ip_addr))

    def _transfer_recieve_data_thread(self):
        if self.is_master:
            g_logger_obj.log("Data recieve thread started for {0}".format(self.slave.ip_addr))
            [sleep_time, packets_list, packet, overhead] = gen_packets(self.ul_transfer_rate,
                    self.ul_transfer_duration, self.ul_traffic_type, self.ul_transfer_protocol,
                    self.mtu_size)
            if self.ul_transfer_protocol==data_protocol_type_enum_t.UDP_TRAFFIC:
                sleep_time = SOCKET_UDP_TIMEOUT
                recv = recv_message_udp
                sock = self.ul_data_channel_udp
            elif self.ul_transfer_protocol==data_protocol_type_enum_t.TCP_TRAFFIC:
                sleep_time = SOCKET_TCP_TIMEOUT
                recv = recv_message_tcp
                sock = self.ul_data_channel_tcp
            else:
                assert 0
            g_logger_obj.log("Receiving {0} packets of size {1} using sleep of {2}".format(len(packets_list),
                packets_list[0], sleep_time))
            seq_idx = 0
            start_time = time()
            sum_data_len = 0
            sum_delta_time = 0
            self.ul_transfer_thread_event.set()
            for idx in range(len(packets_list)):
                recv_start = time()
                msg = recv(sock, sleep_time, is_data_packet=True)
                if msg:
                    data_len = packet_size = len(msg.data_bytes) + overhead + data_msg_pack_size
                    seq_idx += 1
                else:
                    data_len = 0

                if not self.ul_transfer_thread_event.is_set() or not self.slave.is_connected:
                    print("Stopping data transfer since event was set")
                    break

                recv_end = time()
                if sum_delta_time > DATA_RATE_MONITOR_INTERVAL:
                    self.ul_inst_data_rate = sum_data_len/sum_delta_time
                    sum_data_len = 0
                    sum_delta_time = 0
                else:
                    sum_data_len += data_len
                    sum_delta_time += (recv_end - recv_start)
                sleep(0)    # yield to other thread
            self.ul_transfer_thread_event.clear()
            g_logger_obj.log("Data recieve thread stopped for {0}".format(self.slave.ip_addr))
        else:
            g_logger_obj.log("Data recieve thread started for {0}".format(self.master.ip_addr))
            [sleep_time, packets_list, packet, overhead] = gen_packets(self.ul_transfer_rate,
                    self.ul_transfer_duration, self.ul_traffic_type, self.ul_transfer_protocol,
                    self.mtu_size)
            if self.ul_transfer_protocol==data_protocol_type_enum_t.UDP_TRAFFIC:
                sleep_time = SOCKET_UDP_TIMEOUT
                recv = recv_message_udp
                sock = self.dl_data_channel_udp
            elif self.ul_transfer_protocol==data_protocol_type_enum_t.TCP_TRAFFIC:
                sleep_time = SOCKET_TCP_TIMEOUT
                recv = recv_message_tcp
                sock = self.dl_data_channel_tcp
            else:
                assert 0
            seq_idx = 0
            start_time = time()
            sum_data_len = 0
            sum_delta_time = 0
            self.ul_transfer_thread_event.set()
            for idx in range(len(packets_list)):
                recv_start = time()
                msg = recv(sock, sleep_time, is_data_packet=True)
                if msg:
                    data_len = packet_size = len(msg.data_bytes) + overhead + data_msg_pack_size
                    seq_idx += 1
                else:
                    data_len = 0
                    g_logger_obj.log("Recieved none @ idx={0}".format(idx))

                if not self.ul_transfer_thread_event.is_set() or not self.master.is_connected:
                    print("Stopping data transfer since event was set")
                    break

                recv_end = time()
                if sum_delta_time > DATA_RATE_MONITOR_INTERVAL:
                    self.dl_inst_data_rate = sum_data_len/sum_delta_time
                    sum_data_len = 0
                    sum_delta_time = 0
                else:
                    sum_data_len += data_len
                    sum_delta_time += (recv_end - recv_start)
                sleep(0)    # yield to other thread
            self.ul_transfer_thread_event.clear()
            g_logger_obj.log("Data recieve thread stopped for {0}".format(self.master.ip_addr))

    def _transfer_threads_start(self):
        if self.dl_transfer_thread:
            self.dl_transfer_thread.start()
        if self.ul_transfer_thread:
            self.ul_transfer_thread.start()

    def _transfer_threads_running(self):
        if self.dl_transfer_thread:
            if self.dl_transfer_thread.isAlive():
                return True
        if self.ul_transfer_thread:
            if self.ul_transfer_thread.isAlive():
                return True
        return False

    def transfer_query(self):
        if self.is_master:
            self.transfer_action = transfer_action_enum_t.TRANSFER_QUERY
            transfer_ctrl_msg = transfer_ctrl_msg_t(
                    transfer_idx = self.transfer_idx, 
                    transfer_action = self.transfer_action,
                    transfer_action_time = ctime())
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_ctrl_msg)
            send_message_tcp(self.slave.ctrl_channel, py_nwtraffic_msg, 0)

            if self._transfer_threads_running():
                master_status = transfer_action_ack_enum_t.TRANSFER_ONGOING
            else:
                master_status = transfer_action_ack_enum_t.TRANSFER_COMPLETED
            slave_reply = recv_message_tcp(self.slave.ctrl_channel, SOCKET_CTRL_TIMEOUT)
            if slave_reply:
                slave_status = slave_reply.msg_content.transfer_action_ack
                self.dl_reported_inst_data_rate = slave_reply.msg_content.inst_dl_rate
                self.ul_reported_inst_data_rate = slave_reply.msg_content.inst_ul_rate
            else:
                self.dl_reported_inst_data_rate = -1
                self.ul_reported_inst_data_rate = -1
                slave_status = None
            return [master_status, slave_status]
        else:
            self.transfer_action = transfer_action_enum_t.TRANSFER_QUERY
            if self._transfer_threads_running():
                action_ack = transfer_action_ack_enum_t.TRANSFER_ONGOING
            else:
                action_ack = transfer_action_ack_enum_t.TRANSFER_COMPLETED
            g_logger_obj.log("Sending TRANSFER_QUERY {0} as {1}".format(self.master.ip_addr, \
                transfer_action_ack_enum_t._names_[action_ack]))
            transfer_ack_msg = transfer_ack_msg_t(
                    self.transfer_idx,
                    self.transfer_action,
                    ctime(),
                    action_ack,
                    self.dl_inst_data_rate,
                    self.ul_inst_data_rate)
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_ack_msg)
            try:
                send_message_tcp(self.master.ctrl_channel, py_nwtraffic_msg, 0)
                return True
            except:
                return False
            return action_ack

    def transfer_stop(self):
        self.transfer_action = transfer_action_enum_t.TRANSFER_STOP
        if self.is_master:
            transfer_ctrl_msg = transfer_ctrl_msg_t(
                    transfer_idx = self.transfer_idx, 
                    transfer_action = self.transfer_action,
                    transfer_action_time = ctime())
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_ctrl_msg)
            send_message_tcp(self.slave.ctrl_channel, py_nwtraffic_msg, 0)
            return recv_message_tcp(self.slave.ctrl_channel, SOCKET_CTRL_TIMEOUT)
        else:
            self.dl_transfer_thread_event.clear()
            self.ul_transfer_thread_event.clear()
            g_logger_obj.log("Slave : Clearing dl and ul thread event")
            while self._transfer_threads_running():
                pass
            action_ack = transfer_action_ack_enum_t.TRANSFER_SUCCESS
            transfer_ack_msg = transfer_ack_msg_t(
                    self.transfer_idx,
                    self.transfer_action,
                    ctime(),
                    action_ack,
                    self.dl_inst_data_rate,
                    self.ul_inst_data_rate)
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_ack_msg)
            try:
                send_message_tcp(self.master.ctrl_channel, py_nwtraffic_msg, 0)
                return True
            except:
                return False

    def transfer_delete(self):
        self.transfer_action = transfer_action_enum_t.TRANSFER_DELETE
        if self.is_master:
            transfer_ctrl_msg = transfer_ctrl_msg_t(
                    transfer_idx = self.transfer_idx, 
                    transfer_action = self.transfer_action,
                    transfer_action_time = ctime())
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_ctrl_msg)
            send_message_tcp(self.slave.ctrl_channel, py_nwtraffic_msg, 0)
            return recv_message_tcp(self.slave.ctrl_channel, SOCKET_CTRL_TIMEOUT)
        else:
            assert self._transfer_threads_running()==False
            action_ack = transfer_action_ack_enum_t.TRANSFER_SUCCESS
            transfer_ack_msg = transfer_ack_msg_t(
                    self.transfer_idx,
                    self.transfer_action,
                    ctime(),
                    action_ack,
                    self.dl_inst_data_rate,
                    self.ul_inst_data_rate)
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CTRL_MESSAGE,
                msg_content = transfer_ack_msg)
            try:
                send_message_tcp(self.master.ctrl_channel, py_nwtraffic_msg, 0)
                return True
            except:
                return False

def accept_connection(listen_ip, listen_port, timeout):
    s_socket = socket.socket(AF_INET, SOCK_STREAM)
    s_socket.settimeout(SOCKET_CONNECT_TIMEOUT)
    s_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print("accept_connection : Raw TCP bind {0}, {1}".format(listen_ip, listen_port))
    s_socket.bind((listen_ip, listen_port))
    s_socket.listen(1)
    c_socket, c_addr = s_socket.accept()

    if timeout > 0:
        c_socket.settimeout(timeout)

    return (c_socket, c_addr)
    try:
        c_socket, c_addr = s_socket.accept()
        return (c_socket, c_addr)
    except:
        return (None, None)


def create_connection_tcp(server_ip, server_port, timeout, socket_timeout_val = None):
    should_timeout = True if timeout >= 0 else False
    retry_count = 0
    sleep_time = 0.01
    c_socket = socket.socket(AF_INET, SOCK_STREAM)
    while True:
        try:
            print("create_connection_tcp : Raw TCP connect {0}, {1}".format(server_ip, server_port))
            c_socket.connect((server_ip, server_port))
            if socket_timeout_val==None:
                socket_timeout_val = SOCKET_RECV_TIMEOUT
            c_socket.settimeout(socket_timeout_val)
            return c_socket
        except:
            sleep(sleep_time)
            if should_timeout:
                timeout -= sleep_time
                if timeout < 0 :
                    return None

def create_connection_udp(server_ip, server_port, is_receive, timeout, socket_timeout_val = None):
    c_socket = socket.socket(AF_INET, SOCK_DGRAM)
    if is_receive:
        print("create_connection_udp : Raw UDP bind {0}, {1}".format(server_ip, server_port))
        try:
            c_socket.bind((server_ip, server_port))
        except:
            print("socket bind failed at {0}, {1}".format(server_ip, server_port))
            set_trace()
    else:
        pass
    if socket_timeout_val==None:
        socket_timeout_val = SOCKET_RECV_TIMEOUT
    c_socket.settimeout(socket_timeout_val)
    return (c_socket, server_ip, server_port)

def disconnect_connection_udp(sock_ipaddr_port):
    sock_ipaddr_port[0].close()

def disconnect(sock):
    sock.close()

def send_message_udp(sock_ipaddr_port, data, sleep_time, is_data_packet = False):
    (sock, ip_addr, port) = sock_ipaddr_port
    data_packet = generate_packet(data, is_data_packet)
    sent = sock.sendto(data_packet, (ip_addr, port))
    assert sent==len(data_packet)
    if sleep_time > 0 : 
        sleep(sleep_time)
#    stderr.write("Actual sleep of {0}\n".format(sleep_time))
    send_message_udp.cur_time = time()
    return len(data_packet)


def send_message_tcp(sock, data, sleep_time, is_data_packet = False):
    if 'cur_time' not in send_message_tcp.__dict__:
        send_message_tcp.cur_time = time()
    data_packet = generate_packet(data, is_data_packet)
    total_sent = 0
    while total_sent!=len(data_packet):
        sent = sock.send(data_packet[total_sent:])
        total_sent += sent
    sleep_time -= (time()-send_message_tcp.cur_time)
    if sleep_time > 0 : 
        sleep(sleep_time)
    send_message_tcp.cur_time = time()
    return len(data_packet)

def generate_packet(data, is_data_packet):
    if is_data_packet:
        data_array = data_msg_pack(data)
    else:
        data_array = dumps(data)
    msg_len = len(data_array)
    byte_array = pack("@" + SOCKET_DATA_PACK_FORMAT_SPEC, msg_len)
    return byte_array + data_array

def recv_bytes_tcp(sock, num_bytes, timeout):
    total_recieved = 0
    recieved_bytes = bytes()
    while total_recieved < num_bytes:
        [readable, _, _] = select([sock], [], [], timeout)
        if len(readable)==0:
            return None
        recv_bytes_arr = sock.recv(num_bytes-total_recieved)
        if len(recv_bytes_arr)==0:
            raise tcpSocketClosed # Connection closed gracefully
        total_recieved += len(recv_bytes_arr)
        recieved_bytes += recv_bytes_arr
    return recieved_bytes

def recv_bytes_udp(sock, num_bytes):
    total_recieved = 0
    recieved_bytes = bytes()
    while total_recieved < num_bytes:
        recv_bytes_arr = sock.recv(num_bytes-total_recieved)
        assert len(recv_bytes_arr)
        total_recieved += len(recv_bytes_arr)
        recieved_bytes += recv_bytes_arr
    return recieved_bytes


def recv_message_tcp(sock, timeout, is_data_packet = False):
    byte_array = pack("@" + SOCKET_DATA_PACK_FORMAT_SPEC, 0)
    length_field_num_bytes = len(byte_array)
    try:
        length_field_bytes = recv_bytes_tcp(sock, length_field_num_bytes, timeout)
        [msg_len] = unpack("@" + SOCKET_DATA_PACK_FORMAT_SPEC, length_field_bytes)
        data_array = recv_bytes_tcp(sock, msg_len, timeout)
        if is_data_packet:
            return data_msg_unpack(data_array)
        else:
            return loads(data_array)
    except tcpSocketClosed:
        return 0    # Indicate socket closure
    except:
        return None # Indicate waiting timeout

def recv_message_udp(sock_ipaddr_port, timeout, is_data_packet = False, ctrl_mode = False):
    (sock, ip_addr, port) = sock_ipaddr_port
    byte_array = pack("@" + SOCKET_DATA_PACK_FORMAT_SPEC, 0)
    length_field_num_bytes = len(byte_array)
    try:
        [readable, _, _] = select([sock], [], [], timeout)
        if len(readable)==0:
            return None
        [data_array, ip_port] = sock.recvfrom(SOCKET_MAX_PACKET_SIZE)
        if is_data_packet:
            return data_msg_unpack(data_array[calcsize(SOCKET_DATA_PACK_FORMAT_SPEC):])
        else:
            if ctrl_mode:
                return [ip_port[0], ip_port[1], loads(data_array[calcsize(SOCKET_DATA_PACK_FORMAT_SPEC):])]   
            else:
                return loads(data_array)
    except:
        g_logger_obj.log("socket try failed {0}".format(sock_ipaddr_port))
        return None

class ctrl_connect_class_t:
    def __init__(self, is_master):
        self.is_master = is_master

    def ctrl_connect_init(self, slave_ip = None):
        if self.is_master:
            assert slave_ip!=None
            is_receive = False
            self.send_sock = create_connection_udp(slave_ip, connection_port_enum_t.SLAVE_CONNECT_RECV_PORT_UDP, 
                    is_receive, SOCKET_UNLIMITED_TIMEOUT)
            is_receive = True
            self.recv_sock = create_connection_udp("", connection_port_enum_t.MASTER_CONNECT_RECV_PORT_UDP, 
                    is_receive, SOCKET_UNLIMITED_TIMEOUT)
        else:
            [master_ip_addr, port] = self.get_master_ip(SOCKET_UNLIMITED_TIMEOUT)
            assert master_ip_addr!=None
            is_receive = False
            self.send_sock = create_connection_udp(master_ip_addr, connection_port_enum_t.MASTER_CONNECT_RECV_PORT_UDP, 
                    is_receive, SOCKET_UNLIMITED_TIMEOUT)
            is_receive = True
            self.recv_sock = create_connection_udp("", connection_port_enum_t.SLAVE_CONNECT_RECV_PORT_UDP, is_receive, SOCKET_UNLIMITED_TIMEOUT)

    def get_master_ip(self, timeout):
        is_receive = True
        sock = create_connection_udp("", connection_port_enum_t.SLAVE_CONNECT_RECV_PORT_UDP, is_receive, timeout)
        [ip_addr, port, _] = recv_message_udp(sock, timeout, is_data_packet = False, ctrl_mode = True)
        disconnect_connection_udp(sock)
        return [ip_addr, port]

    def send_msg(self, msg):
        return send_message_udp(self.send_sock, msg, 0, is_data_packet = False)

    def recv_msg(self, timeout):
        msg = recv_message_udp(self.recv_sock, timeout, 
            is_data_packet = False, ctrl_mode = True)
        if msg:
            [ip_addr, port, msg] = msg
        return msg

    def disconnect(self):
        disconnect_connection_udp(self.send_sock)
        disconnect_connection_udp(self.recv_sock)

def calculate_sleep_time(total_data, duration, mtu_size):
    num_packets = ceil(total_data/mtu_size)
    sleep_time = duration/num_packets
    return [sleep_time, num_packets]

def gen_packets(data_rate, duration, transfer_type, protocol_type, mtu_size):
    data_rate *= DATA_RATE_SCALE
    total_data = floor(data_rate/8*duration)
    if protocol_type==data_protocol_type_enum_t.UDP_TRAFFIC:
        overhead = ETHERNET_OVERHEAD + IP_PACKET_OVERHEAD + UDP_OVERHEAD
    else:
        overhead = ETHERNET_OVERHEAD + IP_PACKET_OVERHEAD + TCP_OVERHEAD
    [sleep_time, num_packets] = calculate_sleep_time(total_data, duration, mtu_size)
    total_data -= num_packets*(data_msg_pack_size+overhead)
    total_data = total_data if total_data > 0 else 0
    packets_list = list()
    if transfer_type==data_transfer_type_enum_t.RECTANGLE_TRANSFER:
        data_size = ceil(total_data / num_packets)
        for i in range(num_packets):
            packets_list.append(data_size)
        packet = urandom(data_size)
        return [sleep_time, packets_list, packet, overhead]
    elif transfer_type==data_transfer_type_enum_t.SINE_WAVE_TRANSFER:
        peak_packet_size = ceil(total_data / num_packets)
        bottom_packet_size = max(peak_packet_size/10, 1)
        num_packets_cycle = ceil(1/(data_transfer_properties_enum_t.TRIANGLE_WAVE_FREQ)/sleep_time)
        diff_size = peak_packet_size - bottom_packet_size
        for i in range(num_packets):
            packet_size = ceil(bottom_packet_size + diff_size*(1+sin(2*pi*i/num_packets_cycle))/2)
            packets_list.append(packet_size)
        packet = urandom(peak_packet_size)
        return [sleep_time, packets_list, packet, overhead]
    elif transfer_type==data_transfer_type_enum_t.TRIANGLE_TRANSFER:
        peak_packet_size = ceil(total_data / num_packets)
        bottom_packet_size = max(ceil(peak_packet_size/10), 1)
        num_packets_cycle = ceil(1/(data_transfer_properties_enum_t.TRIANGLE_WAVE_FREQ)/sleep_time)
        change_size = ceil((peak_packet_size - bottom_packet_size)/(num_packets_cycle/2))
        packet_size = 0
        for i in range(num_packets):
            i_mod = (i % num_packets_cycle)
            if i_mod==0:
                packet_size = peak_packet_size
            elif i_mod < num_packets_cycle/2:
                packet_size -= change_size
            elif i_mod > num_packets_cycle/2:
                packet_size += change_size
            else:
                packet_size = packet_size
            packets_list.append(packet_size)
        packet = urandom(peak_packet_size)
        return [sleep_time, packets_list, packet, overhead]
    elif transfer_type==data_transfer_type_enum_t.RANDOM_TRANSFER:
        max_packet_size = ceil(total_data / num_packets)
        for i in range(num_packets):
            packets_list.append(randint(1, max_packet_size))
        packet = urandom(max_packet_size)
        return [sleep_time, packets_list, packet, overhead]
    else:
        set_trace()
        assert 0

