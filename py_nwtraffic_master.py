import re
import sys
import signal
import py_nwtraffic_common
from queue import Queue
from pdb import set_trace
from time import sleep, ctime, time
from py_nwtraffic_ctrl import *
from socket import socket
from threading import Thread, Lock
from collections import namedtuple, OrderedDict
from py_nwtraffic_common import *
from pickle import loads


if __name__=='__main__':
    print("Hello World")

class py_nwtraffic_master_t:
    def __init__(self, regex_slave_ip_addr=None):
        self.transfer_idx = 0
        self.slaves = dict()
        py_nwtraffic_common.g_logger_obj = py_nwtraffic_logger_t(is_master=True)
        self.beacon_msgs_edit_lock = Lock()
        self.beacon_msgs = dict()
        self.keep_alive_threads = dict()
        self.logging_threads = dict()
        self.main_thread = None
        self.exit = False
        self.regex_slave_ip_addr = regex_slave_ip_addr
        self.flash_print_dict = dict()
        signal.signal(signal.SIGINT, self.sigint_handler)
        self.print_thread = Thread(target=self.func_background_printing)
        self.print_thread.start()
        self.beacon_thread = Thread(target=self._beacon_recv_thread)
        self.beacon_thread.start()
        self.ipaddr_tcp_socket = socket.socket(AF_INET, SOCK_STREAM)
        self.ipaddr_tcp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        addr = "0.0.0.0"
        port = connection_port_enum_t.MASTER_IP_ADDR_CONN_PORT_TCP
        print("py_nwtraffic_master_t->__init__ : Raw TCP bind {0}, {1}".format(addr, port))
        self.ipaddr_tcp_socket.bind((addr, port))
        self.ipaddr_tcp_socket.listen(connection_port_enum_t.MASTER_IP_ADDR_CONN_LISTEN_COUNT)

    def is_valid_slave_address(self, ip_addr):
        if self.regex_slave_ip_addr:
            if re.match(self.regex_slave_ip_addr, ip_addr):
                return True
            else:
                return False
        else:
             return True



    def sigint_handler(self, signal, frame):
        slaves = self.slaves.values()
        for slave in slaves:
            slave.is_connected = False

    def func_background_printing(self):
        stats_hdrs = list()
        ############################-----####---------------##---##---------------#---------------------###------------####------------##
        dashed_line = "----------------------------------------------------------------------------------------------------------------------"
        stats_hdrs.append(dashed_line)
        stats_hdrs.append("  t_idx  |  Slave: address, idx |    Protocol   |     Traffic type    |  DL Rate (Reprt Rate) |  UL Rate (Reprt Rate) ")
        stats_hdrs.append(dashed_line)
        start_time = time()
        while not self.exit:
            time_hdr = "Time : {0} : {1} seconds".format(ctime(), int(time()-start_time))
            messages_to_print = dict()
            slaves = self.slaves.values()
            total_dl_rate = 0
            total_ul_rate = 0
            for idx, slave in enumerate(slaves):
                slave_idx = slave.idx
                slave_ip_addr = slave.ip_addr
                for transfer_ref in slave.transfer_ref.values():
                    transfer_idx = transfer_ref.transfer_idx
                    is_valid = False
                    if transfer_ref.dl_transfer_thread_event.is_set():
                        is_valid = True
                        transfer_protocol = data_protocol_type_enum_t._names_[transfer_ref.dl_transfer_protocol]
                        transfer_traffic_type = data_transfer_type_enum_t._names_[transfer_ref.dl_traffic_type]
                        dl_data_rate = transfer_ref.dl_inst_data_rate*8/1024/1024
                        reported_dl_data_rate = transfer_ref.dl_reported_inst_data_rate*8/1024/1024
                        total_dl_rate += reported_dl_data_rate
                    else:
                        dl_data_rate = 0
                        reported_dl_data_rate = 0
                    dl_data_rate = "{0:.1f}Mbps".format(dl_data_rate)
                    reported_dl_data_rate = "{0:.1f}Mbps".format(reported_dl_data_rate)
                    if transfer_ref.ul_transfer_thread_event.is_set():
                        is_valid = True
                        transfer_protocol = data_protocol_type_enum_t._names_[transfer_ref.ul_transfer_protocol]
                        transfer_traffic_type = data_transfer_type_enum_t._names_[transfer_ref.ul_traffic_type]
                        ul_data_rate = transfer_ref.ul_inst_data_rate*8/1024/1024
                        reported_ul_data_rate = transfer_ref.ul_reported_inst_data_rate*8/1024/1024
                        total_ul_rate += reported_ul_data_rate
                    else:
                        ul_data_rate = 0
                        reported_ul_data_rate = 0
                    ul_data_rate = "{0:.1f}Mbps".format(ul_data_rate)
                    reported_ul_data_rate = "{0:.1f}Mbps".format(reported_ul_data_rate)
                    if is_valid:
                        messages_to_print[transfer_idx] = "  {0:5d}  | {1:>15s}, {2:3d} | {3:^13s} | {4:^19s} | {5:>9} ({6:>9s}) | {7:>9s} ({8:>9s})".format(
                            transfer_ref.transfer_idx, slave.ip_addr, slave_idx, transfer_protocol, transfer_traffic_type, 
                            dl_data_rate, reported_dl_data_rate, ul_data_rate, reported_ul_data_rate)
            
            total_dl_rate = "{0:.1f}Mbps".format(total_dl_rate)
            total_ul_rate = "{0:.1f}Mbps".format(total_ul_rate)
            if len(messages_to_print):
                screen_clear()
                print(time_hdr)
                for message in stats_hdrs:
                    print(message)
                keys = list(messages_to_print.keys())
                keys.sort()
                for key in keys:
                    print(messages_to_print[key])
                for message in self.flash_print_dict.values():
                    print(message)
                print(dashed_line)
                print(" Total DL : {0:>9s} , Total UL : {1:>9s}".format(total_dl_rate, total_ul_rate))
                print(dashed_line)
                self.display_beacon_slaves()
                print(dashed_line)
            sleep(0.5)

    def display_beacon_slaves(self):
        if len(self.beacon_msgs):
            for name in self.beacon_msgs.keys():
                (ip_addr, _, msg_ctime) = self.beacon_msgs[name]
                print("{0:30} slave available at {1:>15} Last updated {2}".format(name, ip_addr, msg_ctime))
        


    def connect(self, ip_addr_or_name, timeout=1):
        """Connect to the slave with the specified ip_addr.
    
        Args:
            ip_addr (str): The IPv4 address of the slave to connect to.
    
        Returns:
            slave_ref (object) : A reference to the slave object to be operated upon.

        Examples:
            >>> <master_object>.connect("192.168.32.12")
        """

        if re.match(r"^(\d{1,3}\.){3}\d{1,3}$", ip_addr_or_name):
            # Matches an IP Address input
            ip_addr = ip_addr_or_name
        else:
            # Slave name is specified
            ip_addr = None
            name = ip_addr_or_name
            loop_idx = 0
            while True:
                if name in self.beacon_msgs.keys():
                    (ip_addr, msg_time, _) = self.beacon_msgs[name]
                    if (msg_time-time()) < 5:
                        break
                    else:
                        self._beacon_delete_msg(name)
                loop_idx += 1
                print("\n\nWaiting for slave {0} to report IP address, count = {1}".format(name, loop_idx))
                self.display_beacon_slaves()
                sleep(1)
        try:
            assert ip_addr not in self.slaves.keys()
        except:
            print("IP_ADDR not in slaves")
            set_trace()
            pass
        slave = py_nwtraffic_slave_property_t()
        slave.idx = len(self.slaves)+1
        print("Adding slave {0} with IP {1}".format(slave.idx, ip_addr))
        slave.ip_addr = ip_addr
        slave.curr_seq_num = 0
        slave.data_chunk_size = 1024
        slave.alive_timer = 0

        # Attempt init connection req and resp
        ctrl_connect_req = ctrl_connect_req_t(
                slave_idx = slave.idx,
                slave_ctrl_port = connection_port_enum_t.SLAVE_CTRL_PORT + slave.idx,
                slave_log_port = connection_port_enum_t.SLAVE_LOG_PORT + slave.idx,
                slave_alive_port = connection_port_enum_t.SLAVE_ALIVE_PORT + slave.idx,
                req_time = ctime())
        py_nwtraffic_msg = py_nwtraffic_msg_t(
                msg_type = msg_type_enum_t.CONNECT_MESSAGE,
                msg_content = ctrl_connect_req)
       
        ctrl_connect = ctrl_connect_class_t(is_master=True)
        ctrl_connect.ctrl_connect_init(ip_addr)
        print("Control channel connected")
        while True:
            ctrl_connect.send_msg(py_nwtraffic_msg)
            msg_reply = ctrl_connect.recv_msg(1)
            if msg_reply:
                ctrl_connect_rsp = msg_reply.msg_content
                if ctrl_connect_rsp.slave_ack==connect_rsp_ack_enum_t.CONNECT_ACCEPT:
                    py_nwtraffic_msg = py_nwtraffic_msg_t(
                            msg_type = msg_type_enum_t.CONNECT_MESSAGE,
                            msg_content = ctrl_connect_rsp)
                    ctrl_connect.send_msg(py_nwtraffic_msg)
                    ctrl_connect.disconnect()
                    break
                else:
                    assert 0
            else:
                print("\n\nPinging slave {0}".format(ip_addr))
                self.display_beacon_slaves()
        sleep(0.5)
        slave_ref = self._connect_to_slave_ip(slave, ip_addr)
        assert slave_ref!=None
        self.keep_alive_threads[ip_addr] = Thread(target=self._keep_alive_thread, args=(slave,))
        self.logging_threads[ip_addr] = Thread(target=self._logging_thread, args=(slave,))
        self.keep_alive_threads[ip_addr].start()
        self.logging_threads[ip_addr].start()
        self.slaves[ip_addr] = slave_ref
        return slave_ref

    def disconnect(self, slave):
        """Disconnect the slave given as parameter to the function.
    
        Args:
            slave_ref (object): The slave object which needs to be disconnected.
    
        Examples:
            >>> <master_object>.disconnect(<slave_object>)
        """
#        ctrl_disconnect_req = ctrl_disconnect_req_t(
#                slave_idx = 0,
#                req_time = ctime()
#            )
#        py_nwtraffic_msg = py_nwtraffic_msg_t(
#                msg_type = msg_type_enum_t.DISCONNECT_MESSAGE,
#                msg_content = ctrl_disconnect_req
#            )
#        sent_len = send_message_tcp(slave.ctrl_channel, py_nwtraffic_msg, 0) 
        slave.disconnect()

    def transfer_data(self, slave, transfer_protocol, transfer_direction, traffic_type, 
            transfer_duration, transfer_rate, mtu_size, bg=False):
        # Transfer data given the ip address of slave, defined by the direction,
        # protocol, data_rate and duration
        self.transfer_idx += 1
        transfer_ref = py_nwtraffic_transfer_t(is_master=True, master_slave_ref=slave)
        transfer_ref.init(self.transfer_idx, transfer_protocol, transfer_direction, traffic_type,
                transfer_duration, transfer_rate, mtu_size)
        transfer_ref.connect_data_channels()
        self.slaves[slave.ip_addr].transfer_ref[transfer_ref.transfer_idx] = transfer_ref
        transfer_ref.transfer_start()
        if bg:
            return
        else:
            while not self.slave_transfer_completed(slave):
                sleep(1)
    
    def wait_for_transfers(self):
        while True:
            completed = True
            for slave_name, slave in self.slaves.items():
                if not self.slave_transfer_completed(slave):
                    completed = False
            if completed:
                break
            else:
                sleep(1)
    
    def disconnect_slaves(self):
        for slave_name, slave in self.slaves.items():
            slave.disconnect()


    def slave_transfer_completed(self, slave):
        if len(slave.transfer_ref):
            sleep(5)
            del_transfer_idx = list()
            transfer_completed = False
            for transfer_ref in slave.transfer_ref.values():
                sleep(0.1)
                [master_status, slave_status] = transfer_ref.transfer_query()
                if master_status==transfer_action_ack_enum_t.TRANSFER_COMPLETED or \
                        slave_status==transfer_action_ack_enum_t.TRANSFER_COMPLETED:
                    if slave_status!=None:
                        py_nwtraffic_common.g_logger_obj.log("Stopping slave transfer ; {0}".format(slave_status))
                        transfer_ref.transfer_delete()
                    del_transfer_idx.append(transfer_ref.transfer_idx)
                    transfer_completed = True
            if transfer_completed:
                for idx in del_transfer_idx:
                    del slave.transfer_ref[idx]
        if len(slave.transfer_ref):
            return False
        else:
            return True


    def _keep_alive_thread(self, slave):
#        print("Keep alive thread started for {0}".format(slave.ip_addr))
        py_nwtraffic_common.g_logger_obj.log("Keep alive thread started for {0}".format(slave.ip_addr))
        send_seq_idx = 0
        num_missed_msgs = 0
        while slave.is_connected and not self.exit:
            keep_alive_msg = keep_alive_msg_t(
                    seq_idx = send_seq_idx,
                    cur_time = ctime()
                )
            py_nwtraffic_msg = py_nwtraffic_msg_t(
                    msg_type = msg_type_enum_t.MASTER_TO_SLAVE_ALIVE_MESSAGE,
                    msg_content = keep_alive_msg
                )
            try:
                sent_len = send_message_tcp(slave.alive_channel, py_nwtraffic_msg, 0) 
            except:
                num_missed_msgs += 1

            assert sent_len
            recv_msg = recv_message_tcp(slave.alive_channel, SLAVE_ALIVE_MESSAGE_INTERVAL*2)
            if recv_msg==None:
                num_missed_msgs += 1
                if num_missed_msgs > NUM_IGNORE_SLAVE_MISSED_ALIVE_MSGS:
                    slave.is_connected = False
                    print("slave : disconnected {0} because of missing keep_alive messages".format(slave.ip_addr))
                    for transfer_ref in slave.transfer_ref.values():
                        transfer_ref.dl_transfer_thread_event.clear()
                        transfer_ref.ul_transfer_thread_event.clear()
                    break
            else:
                num_missed_msgs = 0
                send_seq_idx += 1
        py_nwtraffic_common.g_logger_obj.log("Keep alive thread stopped for {0}".format(slave.ip_addr))
#        print("Keep alive thread stopped for {0}".format(slave.ip_addr))
        

    def _logging_thread(self, slave):
#        print("Logging thread started for {0}".format(slave.ip_addr))
#        print("Logging thread stopped for {0}".format(slave.ip_addr))
        pass


    def _connect_to_slave_ip(self, slave, ip_addr):
        all_ip = ""
        slave_idx = slave.idx
        slave.ctrl_channel = slave.logging_channel = slave.alive_channel = None
        while True:
            if slave.ctrl_channel==None:
                slave.ctrl_channel          =  \
                    create_connection_tcp(ip_addr, connection_port_enum_t.SLAVE_CTRL_PORT + slave_idx, -1)
                sleep(SOCKET_CONNECT_DELAY)
            if slave.logging_channel==None:
                slave.logging_channel       =  \
                    create_connection_tcp(ip_addr, connection_port_enum_t.SLAVE_LOG_PORT + slave_idx, 5)
                sleep(SOCKET_CONNECT_DELAY)
            if slave.alive_channel==None:
                slave.alive_channel         =  \
                    create_connection_tcp(ip_addr, connection_port_enum_t.SLAVE_ALIVE_PORT + slave_idx, 5)
                sleep(SOCKET_CONNECT_DELAY)

            if slave.ctrl_channel != None and slave.logging_channel != None and slave.alive_channel != None:
                break;

        slave.alive_timer   = 0
        slave.is_connected  = True
        return slave
    
    def _beacon_recv_thread(self):
        py_nwtraffic_common.g_logger_obj.log("Starting _beacon_recv_thread")
        sock = socket.socket(AF_INET, SOCK_DGRAM)
        addr = "0.0.0.0"; port = connection_port_enum_t.MASTER_UDP_BEACON_MSG_RECV_PORT 
        print("py_nwtraffic_master_t->_beacon_recv_thread : Raw UDP bind {0}, {1}".format(addr, port))
        sock.bind((addr, port))
        sock.settimeout(1)
        while not self.exit:
            try:
                [readable, _, _] = select([sock], [], [], BEACON_MSG_INTERVAL)
                [data, (ip_addr, port)] = sock.recvfrom(SOCKET_MAX_PACKET_SIZE)
                msg = loads(data)
                ip_addresses = msg.beacon_slave_ip_addresses
                if  not self.is_valid_slave_address(ip_addr):
                    for ip_addr in ip_addresses:
                        if  self.is_valid_slave_address(ip_addr):
                            break
                if  self.is_valid_slave_address(ip_addr):
                    self._beacon_create_msg(msg.beacon_slave_name, ip_addr)

            except socket.timeout:
                sleep(1)
        py_nwtraffic_common.g_logger_obj.log("Stopping _beacon_recv_thread")

 
    def _beacon_delete_msg(self, name):
        self.beacon_msgs_edit_lock.acquire()
        del self.beacon_msgs[name]
        self.beacon_msgs_edit_lock.release()

    def _beacon_create_msg(self, name, ip_addr):
        self.beacon_msgs_edit_lock.acquire()
        self.beacon_msgs[name] = (ip_addr, time(), ctime())
        self.beacon_msgs_edit_lock.release()


