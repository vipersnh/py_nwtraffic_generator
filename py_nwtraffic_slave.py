import errno
import sys
import signal
import py_nwtraffic_common
from queue import Queue
from pdb import set_trace
from socket import socket
from threading import Thread, Lock
from collections import namedtuple
from py_nwtraffic_common import *
from py_nwtraffic_ctrl import *
from time import time, ctime
from pickle import dumps

class py_nwtraffic_slave_t:
    def __init__(self, slave_name, master_ip_address):
        self.master = py_nwtraffic_master_property_t()
        self.transfer_delete_lock = Lock()
#        signal.signal(signal.SIGINT, self.sigint_handler)
        self.slave_idx = 0
        self.slave_ctrl_port = None
        self.slave_dl_data_port_tcp = None
        self.slave_ul_data_port_tcp = None
        self.slave_dl_data_port_udp = None   # Master transmit, slave recieve 
        self.master_ul_data_port_udp = None  # Master recieve, slave transmit
        self.slave_log_port = None
        self.slave_alive_port = None
        self.connect_req = None
        self.name = slave_name
        self.master_ip_address = master_ip_address
        self.beacon_thread = Thread(target=self._beacon_thread, args=(self.master,))
        py_nwtraffic_common.g_logger_obj = py_nwtraffic_logger_t(is_master=False)


    def sigint_handler(self, signal, frame):
        self.master.is_connected = False

    def start(self):
        print("Starting {0} to connect to master @ {1}".format(self.name, self.master_ip_address))
        self.beacon_thread.start()
        while True:
            print("Waiting for ctrl_connect_req_t")
            ctrl_connect = ctrl_connect_class_t(is_master=False)
            ctrl_connect.ctrl_connect_init(None)
            while True:
                msg = ctrl_connect.recv_msg(SOCKET_UNLIMITED_TIMEOUT)
                if g_debug_mode_print:
                    print("Slave received ctrl_connect_req_t : {0}".format(msg))
                ctrl_connect_req = msg.msg_content
                self.connect_req = ctrl_connect_req
                ctrl_connect_rsp = ctrl_connect_rsp_t(
                        slave_idx = ctrl_connect_req.slave_idx,
                        slave_ctrl_port = ctrl_connect_req.slave_ctrl_port,
                        slave_ack = connect_rsp_ack_enum_t.CONNECT_ACCEPT, 
                        rsp_time = ctime())
                py_nwtraffic_msg = py_nwtraffic_msg_t(
                        msg_type = msg_type_enum_t.CONNECT_MESSAGE, msg_content = ctrl_connect_rsp)
                ctrl_connect.send_msg(py_nwtraffic_msg)
                msg = ctrl_connect.recv_msg(5);

                if g_debug_mode_print:
                    print("Slave received ctrl_connect_rsp_t : {0}".format(msg))

                if msg:
                    ctrl_connect_rsp = msg.msg_content
                    if type(ctrl_connect_rsp)==ctrl_connect_rsp_t:
                        if ctrl_connect_rsp.slave_ack==connect_rsp_ack_enum_t.CONNECT_ACCEPT:
                            self.slave_idx = self.connect_req.slave_idx
                            self.slave_ctrl_port = self.connect_req.slave_ctrl_port
                            self.slave_log_port = self.connect_req.slave_log_port
                            self.slave_alive_port = self.connect_req.slave_alive_port
                            if g_debug_mode_print:
                                print("Slave starting _connect_to_master_control_path")
                            master_ref = self._connect_to_master_control_path()
                            if master_ref:
                                break
                        else:
                            pass
                

            screen_clear()
            print("Connected to master")
            master_ref.keep_alive_thread = Thread(target=self._keep_alive_thread, args=(master_ref,))
            master_ref.logging_thread = Thread(target=self._logging_thread, args=(master_ref,))
            master_ref.keep_alive_thread.start()
            master_ref.logging_thread.start()
            while master_ref.is_connected:
                self.process_command(master_ref)
            
            self.disconnect_master(master_ref)
            
    def disconnect_master(self, master):
        self.transfer_delete_lock.acquire()
        self._delete_transfer_ref(master)
        self.transfer_delete_lock.release()
        print("Disconnected from master")
        sleep(1)

    def process_command(self, master):
        msg = self._recv_ctrl_msg_from_master(master)
        py_nwtraffic_common.g_logger_obj.log(str(msg))
        if msg!=None:
            self.transfer_delete_lock.acquire()
            if msg.msg_type==msg_type_enum_t.CTRL_MESSAGE:
                cmd = msg.msg_content
                if type(cmd)==transfer_req_msg_t:
                    transfer_ref = py_nwtraffic_transfer_t(is_master=False, master_slave_ref=self.master)
                    transfer_ref.init(cmd.transfer_idx, cmd.data_protocol, cmd.data_direction,
                            cmd.data_transfer_type, cmd.data_duration, cmd.data_transfer_rate,
                            cmd.mtu_size)
                    assert transfer_ref.transfer_idx not in master.transfer_ref.keys()
                    master.transfer_ref[transfer_ref.transfer_idx] = transfer_ref
                    transfer_ref.connect_data_channels()
                    print("Recieved TRANSFER INIT MESSAGE {0}".format(master.ip_addr))
                elif type(cmd)==transfer_ctrl_msg_t:
                    try:
                        transfer_ref = master.transfer_ref[cmd.transfer_idx]
                    except:
                        self._delete_transfer_ref(master)
                        master.is_connected = False   
                    if cmd.transfer_action==transfer_action_enum_t.TRANSFER_START:
                        print("Recieved TRANSFER_START {0}, transfer_idx {1}".format(master.ip_addr, cmd.transfer_idx))
                        transfer_ref.transfer_start()
                    elif cmd.transfer_action==transfer_action_enum_t.TRANSFER_QUERY:
                        #print("Recieved TRANSFER_QUERY {0}, transfer_idx {1}".format(master.ip_addr, cmd.transfer_idx))
                        transfer_ref.transfer_query()
                    elif cmd.transfer_action==transfer_action_enum_t.TRANSFER_STOP:
                        print("Recieved TRANSFER_STOP {0}, transfer_idx {1}".format(master.ip_addr, cmd.transfer_idx))
                        transfer_ref.transfer_stop()
                    elif cmd.transfer_action==transfer_action_enum_t.TRANSFER_DELETE:
                        print("Recieved TRANSFER_DELETE {0}, transfer_idx {1}".format(master.ip_addr, cmd.transfer_idx))
                        transfer_ref.transfer_delete()
                        self._delete_transfer_ref(master, transfer_ref)
                    else:
                        assert 0
            elif msg.msg_type==msg_type_enum_t.DISCONNECT_MESSAGE:
                cmd = msg.msg_content
                if type(cmd)==ctrl_disconnect_req_t:
                    master.is_connected = False
                    print("Disconnect request received")
                else:
                    set_trace()
            else:
                set_trace()
            self.transfer_delete_lock.release()

    def _connect_to_master_control_path(self):
        all_ip = ""
        print("Listening for TCP : CTRL  channel {0}".format(self.slave_ctrl_port))
        [self.master.ctrl_channel, [self.master.ip_addr, _port]] =  \
            accept_connection(all_ip, self.slave_ctrl_port, -1)
        print("Listening for TCP : LOG   channel {0}".format(self.slave_log_port))
        [self.master.logging_channel, _] =  \
            accept_connection(all_ip, self.slave_log_port, -1)
        print("Listening for TCP : ALIVE  channel {0}".format(self.slave_alive_port))
        [self.master.alive_channel, _]   =  \
            accept_connection(all_ip, self.slave_alive_port, -1)

        if self.master.ctrl_channel!=None and self.master.logging_channel!=None \
                and self.master.alive_channel!=None:
            self.master.is_connected = True
            return self.master
        else:
         return None

    def _delete_transfer_ref(self, master, transfer_ref=None):
        transfer_ref_list = list()
        delete_idx = None
        if transfer_ref == None:
            for transfer_ref in master.transfer_ref.values():
                transfer_ref_list.append(transfer_ref)
        else:
            delete_idx = transfer_ref.transfer_idx
            transfer_ref_list.append(transfer_ref)
        for idx in range(len(transfer_ref_list)):
            transfer_ref = transfer_ref_list[idx]
            transfer_ref.dl_transfer_thread_event.clear()
            transfer_ref.ul_transfer_thread_event.clear()
            if delete_idx!=None:
                if transfer_ref.transfer_idx==delete_idx:
                    del master.transfer_ref[transfer_ref.transfer_idx]
            else:
                del master.transfer_ref[transfer_ref.transfer_idx]
        del transfer_ref_list

    def _keep_alive_thread(self, master):
        py_nwtraffic_common.g_logger_obj.log("Keep alive thread started for {0}".format(master.ip_addr))
        num_missed_msgs = 0
        while master.is_connected:
            try:
                recv_msg = self._recv_alive_msg_from_master(master)
                if recv_msg==None:
                    raise
                keep_alive_msg = keep_alive_msg_t(
                        seq_idx = recv_msg.msg_content.seq_idx,
                        cur_time = ctime()
                    )
                py_nwtraffic_msg = py_nwtraffic_msg_t(
                        msg_type = msg_type_enum_t.SLAVE_TO_MASTER_ALIVE_MESSAGE,
                        msg_content = keep_alive_msg
                    )
                self._send_alive_resp_to_master(master, py_nwtraffic_msg)
                num_missed_msgs = 0
            except:
                num_missed_msgs += 1
                if num_missed_msgs > NUM_IGNORE_SLAVE_MISSED_ALIVE_MSGS:
                    master.is_connected = False
                    print("master : disconnected {0} because of missing keep_alive messages".format(master.ip_addr))
                    self.transfer_delete_lock.acquire()
                    self._delete_transfer_ref(master, None)
                    self.transfer_delete_lock.release()
                    break
            sleep(SLAVE_ALIVE_MESSAGE_INTERVAL)
 
        py_nwtraffic_common.g_logger_obj.log("Keep alive thread stopped for {0}".format(master.ip_addr))
        

    def _beacon_thread(self, master):
        py_nwtraffic_common.g_logger_obj.log("Beacon thread started")

        # Obtain IP address using TCP socket connection method
        print("Trying to obtain self IP address using TCP connection method")
        while True:
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                print("py_nwtraffic_slave_t->_beacon_thread : Raw TCP connect {0}, {1}".format(self.master_ip_address, connection_port_enum_t.MASTER_IP_ADDR_CONN_PORT_TCP))
                sock.connect((self.master_ip_address, connection_port_enum_t.MASTER_IP_ADDR_CONN_PORT_TCP))
                break
            except:
                sleep(1)
        print("Obtained connection with details : {0}".format(sock.getsockname()))
        ip_addresses = tuple([sock.getsockname()[0]])
        sock.close()
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        while True:
            beacon_msg = beacon_msg_t(beacon_slave_name = self.name,
                    beacon_slave_ip_addresses = ip_addresses)
            try:
                sock.sendto(dumps(beacon_msg), 
                        (self.master_ip_address, connection_port_enum_t.MASTER_UDP_BEACON_MSG_RECV_PORT))
            except socket.error as serr:
                if serr.errno!=errno.ENETUNREACH and serr.errno!=errno.EPERM:
                    raise serr
            sleep(BEACON_MSG_INTERVAL)
        py_nwtraffic_common.g_logger_obj.log("Beacon thread stopped")


    def _logging_thread(self, master):
        pass

    def _recv_ctrl_msg_from_master(self, master):
        py_nwtraffic_common.g_logger_obj.log("Slave : Waiting for ctrl_msg from master")
        msg = None
        while master.is_connected:
            recv_msg = recv_message_tcp(master.ctrl_channel, SOCKET_CTRL_TIMEOUT)
            if recv_msg:
                msg = recv_msg
                break
            else:
                sleep(0.5)
                pass # Try again for another timeout
        return msg

    def _recv_alive_msg_from_master(self, master):
        alive_msg = recv_message_tcp(master.alive_channel, SLAVE_ALIVE_MESSAGE_INTERVAL*4)
        return alive_msg


    def _send_alive_resp_to_master(self, master, msg):
        send_message_tcp(master.alive_channel, msg, 0)

