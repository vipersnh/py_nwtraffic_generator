import sys
from py_nwtraffic_master import *
from py_nwtraffic_ctrl import *
from py_nwtraffic_common import *
from time import time, sleep

# Example transfer steps
#    1. Connec to all slaves
#    2. Transfer to individual slaves with max data rate of 3Mbps in DL path for 30 seconds.
#    3. Transfer to individual slaves with max data rate of 4Mbps in both path for 40 seconds.
#    4. Transfer to all slaves with max data rate of 4Mbps in UL path, simultaneously for 15 seconds.


# Step 1
master = py_nwtraffic_master_t()
slave = master.connect("Any name identifier")
data_rate = 25*1024*1024 # Bits per second
duration  = 100  # Seconds
start_time = time()

transfer_interval_time = 0.01

master.transfer_data(slave, data_protocol_type_enum_t.UDP_TRAFFIC, data_direction_enum_t.MASTER_TO_SLAVE, \
        data_transfer_type_enum_t.RECTANGLE_TRANSFER, duration, data_rate, 1500, bg = True);
print("Waiting for the transfers to complete")
master.wait_for_transfers()
master.disconnect_slaves()
master.exit = True
print("Completed in {0} seconds".format(time()-start_time))
