from pdb import set_trace
from struct import pack, unpack, calcsize

g_debug_mode_print = 0

class Enum:
    def __setattr_(self, name, value):
        return;

slave_alive_timer_interval = 1 # Seconds
slave_alive_timer_max = 10 # Seconds

sine_wave_period = 20 # Seconds
triangle_wave_period = 20 # Seconds

class data_protocol_type_enum_t(Enum):
    UDP_TRAFFIC     = 0
    TCP_TRAFFIC     = 1
    SCTP_TRAFFIC    = 2
    _names_ = [ "UDP_TRAFFIC",
                "TCP_TRAFFIC",
                "SCTP_TRAFFIC",
                ]

class data_transfer_type_enum_t(Enum):
    RECTANGLE_TRANSFER = 0
    SINE_WAVE_TRANSFER = 1
    TRIANGLE_TRANSFER  = 2
    RANDOM_TRANSFER    = 3
    _names_ = [ "RECTANGLE_TRANSFER",
                "SINE_WAVE_TRANSFER",
                "TRIANGLE_TRANSFER",
                "RANDOM_TRANSFER",
                ]

class data_direction_enum_t(Enum):
    MASTER_TO_SLAVE = 0
    SLAVE_TO_MASTER = 1
    BOTH_TO_BOTH    = 2
    _names_ = [ "MASTER_TO_SLAVE",
                "SLAVE_TO_MASTER",
                "BOTH_TO_BOTH",
                ]

class msg_type_enum_t(Enum):
    CONNECT_MESSAGE = 0
    CTRL_MESSAGE  = 1
    DATA_MESSAGE  = 2
    SLAVE_TO_MASTER_LOG_MESSAGE = 3
    SLAVE_TO_MASTER_ALIVE_MESSAGE = 4
    MASTER_TO_SLAVE_ALIVE_MESSAGE = 5
    ABORT_RESET_MESSAGE = 6
    DISCONNECT_MESSAGE = 7

class transfer_action_enum_t(Enum):
    TRANSFER_INIT   = 0
    TRANSFER_START  = 1
    TRANSFER_STOP   = 2
    TRANSFER_DELETE = 3
    TRANSFER_QUERY  = 4
    _names_ = [ "TRANSFER_INIT",
                "TRANSFER_START",
                "TRANSFER_STOP",
                "TRANSFER_DELETE",
                "TRANSFER_QUERY",
                ]

class transfer_action_ack_enum_t(Enum):
    TRANSFER_SUCCESS = 0
    TRANSFER_FAILURE = 1
    TRANSFER_ONGOING = 2
    TRANSFER_COMPLETED = 3
    _names_ = [ "TRANSFER_SUCCESS",
                "TRANSFER_FAILURE",
                "TRANSFER_ONGOING",
                "TRANSFER_COMPLETED",
                ]


class connect_rsp_ack_enum_t(Enum):
    CONNECT_ACCEPT = 0
    CONNECT_REJECT = 1
    _names_ = [ "CONNECT_ACCEPT",
                "CONNECT_REJECT",
                ]

class beacon_msg_t:
    def __init__(self, beacon_slave_name, beacon_slave_ip_addresses):
        self.beacon_slave_name = beacon_slave_name
        self.beacon_slave_ip_addresses = beacon_slave_ip_addresses

class ctrl_connect_req_t:
    def __init__(self, slave_idx, slave_ctrl_port, slave_log_port, slave_alive_port, req_time):
        self.slave_idx = slave_idx
        self.slave_ctrl_port = slave_ctrl_port
        self.slave_log_port = slave_log_port
        self.slave_alive_port = slave_alive_port
        self.req_time = req_time

class ctrl_connect_rsp_t:
    def __init__(self, slave_idx, slave_ctrl_port, slave_ack, rsp_time):
        self.slave_idx = slave_idx
        self.slave_ctrl_port = slave_ctrl_port
        self.slave_ack = slave_ack
        self.rsp_time = rsp_time

class data_connect_req_t:
    def __init__(self, slave_idx, transfer_idx, req_time, master_ul_recv_port_udp, master_ul_recv_port_tcp, slave_dl_recv_port_udp, slave_dl_recv_port_tcp):
        self.slave_idx = slave_idx
        self.transfer_idx = transfer_idx
        self.req_time = req_time
        self.master_ul_recv_port_udp = master_ul_recv_port_udp
        self.master_ul_recv_port_tcp = master_ul_recv_port_tcp
        self.slave_dl_recv_port_udp = slave_dl_recv_port_udp
        self.slave_dl_recv_port_tcp = slave_dl_recv_port_tcp

class data_connect_resp_t:
    def __init__(self, slave_idx, slave_ack, rsp_time):
        self.slave_idx = slave_idx
        self.slave_ack = slave_ack
        self.rsp_time = rsp_time

class transfer_req_msg_t:
    def __init__(self, transfer_idx, data_protocol, data_direction, data_transfer_type, data_duration, data_transfer_rate, max_buffer_size, mtu_size):
        self.transfer_idx = transfer_idx
        self.data_protocol = data_protocol
        self.data_direction = data_direction
        self.data_transfer_type = data_transfer_type
        self.data_duration = data_duration
        self.data_transfer_rate = data_transfer_rate
        self.max_buffer_size = max_buffer_size
        self.mtu_size = mtu_size

class transfer_ctrl_msg_t:
    def __init__(self, transfer_idx, transfer_action, transfer_action_time):
        self.transfer_idx = transfer_idx
        self.transfer_action = transfer_action
        self.transfer_action_time = transfer_action_time

class transfer_ack_msg_t:
    def __init__(self, transfer_idx, transfer_action, transfer_action_time, transfer_action_ack, inst_dl_rate, inst_ul_rate):
        self.transfer_idx = transfer_idx
        self.transfer_action = transfer_action
        self.transfer_action_time = transfer_action_time
        self.transfer_action_ack = transfer_action_ack
        self.inst_dl_rate = inst_dl_rate
        self.inst_ul_rate = inst_ul_rate

class data_msg_t:
    def __init__(self, seq_idx, cur_time, data_bytes):
        self.seq_idx = seq_idx
        self.cur_time = cur_time
        self.data_bytes = data_bytes

class keep_alive_msg_t:
    def __init__(self, seq_idx, cur_time):
        self.seq_idx = seq_idx
        self.cur_time = cur_time

class py_nwtraffic_msg_t:
    def __init__(self, msg_type, msg_content):
        self.msg_type = msg_type
        self.msg_content = msg_content

class ctrl_disconnect_req_t:
    def __init__(self, slave_idx, req_time):
        self.slave_idx = slave_idx
        self.req_time = req_time

data_msg_pack_format = ["I", "30s"]
data_msg_pack_size = calcsize("".join(data_msg_pack_format))

def data_msg_pack(msg):
    return pack("@" + "".join(data_msg_pack_format), msg.seq_idx, msg.cur_time.encode()) + msg.data_bytes


def data_msg_unpack(byte_arr):
    [seq_idx, cur_time] = unpack("@" + "".join(data_msg_pack_format), byte_arr[:data_msg_pack_size])
    data_msg = data_msg_t(seq_idx, cur_time.decode(), byte_arr[data_msg_pack_size:])
    return data_msg

